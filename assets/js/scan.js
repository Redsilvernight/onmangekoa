document.getElementById('start-button').addEventListener('click', function () {
  Quagga.start();
});

Quagga.init({
  inputStream: {
    name: "Live",
    type: "LiveStream",
    target: document.querySelector('#interactive')    // Or '#yourElement' (optional)
  },
  decoder: {
    readers: [
        "code_128_reader",
        "ean_reader",
        "ean_8_reader",
        "codabar_reader",
    ],
    debug: {
        showCanvas: true,
        showPatches: true,
        showFoundPatches: true,
        showSkeleton: true,
        showLabels: true,
        showPatchLabels: true,
        showRemainingPatchLabels: true,
        boxFromPatches: {
            showTransformed: true,
            showTransformedBox: true,
            showBB: true
        }
    }
  },
}, function(err) {
  if (err) {
    return
}
  Quagga.start();
});

Quagga.onProcessed(function (result) {
  var drawingCtx = Quagga.canvas.ctx.overlay,
  drawingCanvas = Quagga.canvas.dom.overlay;

  if (result) {
    if (result.boxes) {
      drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
      result.boxes.filter(function (box) {
        return box !== result.box;
      }).forEach(function (box) {
        Quagga.ImageDebug.drawPath(box, { x: 0, y: 1 }, drawingCtx, { color: "green", lineWidth: 2 });
      });
    }

    if (result.box) {
      Quagga.ImageDebug.drawPath(result.box, { x: 0, y: 1 }, drawingCtx, { color: "#00F", lineWidth: 2 });
    }

    if (result.codeResult && result.codeResult.code) {
      Quagga.ImageDebug.drawPath(result.line, { x: 'x', y: 'y' }, drawingCtx, { color: 'red', lineWidth: 3 });
    }
  }
});

Quagga.onDetected(function (result) {
  var code = result.codeResult.code;
  var checksum = 0;
  
  var codeAccepted = ["3", "4", "6", "8"];

  console.log(code[0]);
  if(code.length > 10) {
    codeAccepted.forEach(startNumber => {
      if (code[0] == startNumber) {
        searchProduct(code);
      } else {
        checksum += 1;
      }
    });

    if (checksum == codeAccepted.length) {
      var text = "Code barre inexploitable";
      
      element.innerHTML = text;
    }
  }
});

function searchProduct(code) {
  var text = "Code barre n°" + code;
  var url = 'https://127.0.0.1:8000/product/detect/' + code;
  var element = document.getElementById("textCode");

  console.log(url);
  element.innerHTML = text;
  window.location = url;

  Quagga.stop();
}


