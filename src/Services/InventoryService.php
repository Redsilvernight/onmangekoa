<?php

namespace App\Services;

use App\Entity\Inventory;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class InventoryService
{
    public function __construct(
        private EntityManagerInterface $manager,
        private UserRepository $userRepository
    ) {
    }

    public function createInventory(): Inventory
    {
        $inventory = new Inventory();

        $this->manager->persist($inventory);
        $this->manager->flush();

        $inventory->setInvitationCode($this->generateCode($inventory));

        $this->manager->flush();

        return $inventory;
    }

    public function generateCode(Inventory $inventory): string
    {
        return $inventory->getId().substr(strval(time()), 3);
    }

    public function removeUser(User $user): void
    {
        $user->setInventory($this->createInventory());
        $this->manager->flush();
    }

    public function listUser(User $user): array
    {
        return $this->userRepository->findBy(['Inventory' => $user->getInventory()->getId()]);
    }

    public function setLimitDate(Inventory $inventory, Int $limit): void
    {
        $inventory->setLimitDate($limit);

        $this->manager->flush();
    }
}
