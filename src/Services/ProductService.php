<?php

namespace App\Services;

use App\Entity\Ingredient;
use App\Repository\IngredientRepository;
use Doctrine\ORM\EntityManagerInterface;

class ProductService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private TagService $tagService,
        private IngredientRepository $ingredientRepository
    ) {
    }

    public function detect(int $code): array
    {
        $curl = curl_init();
        $url = 'https://world.openfoodfacts.org/api/v2/product/'.$code.'.json';

        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo 'cURL Error #:'.$err;
        } else {
            $response = json_decode($response, true);
        }

        return ['code' => $code, 'brand' => $response['product']['brands'], 'product_name' => $response['product']['product_name_fr'], 'image' => $response['product']['image_url'], 'description' => array_key_exists("['generic_name_fr']", $response['product']) ? $response['product']['generic_name_fr'] : '', 'categories' => $response['product']['categories_tags']];
    }

    public function createProduct(array $productInfo): Ingredient
    {
        $existingProduct = self::productExist($productInfo['code']);

        if (null != $existingProduct) {
            return $existingProduct;
        } else {
            $ingredient = new Ingredient();
            $ingredient->setBarecode($productInfo['code'])
                ->setBrand($productInfo['brand'])
                ->setName($productInfo['product_name'])
                ->setDescription($productInfo['description'])
                ->setImageTemp($productInfo['image']);

            $ingredient = self::setTag($ingredient, $productInfo);

            $this->entityManager->persist($ingredient);
            $this->entityManager->flush();

            return $ingredient;
        }
    }

    private function setTag(Ingredient $ingredient, array $productInfo): Ingredient
    {
        foreach ($productInfo['categories'] as $tag) {
            $productTag = $this->tagService->createTag(substr($tag, 3));

            $ingredient->addTag($productTag);
        }

        return $ingredient;
    }

    public function productExist(string $code): mixed
    {
        return $this->ingredientRepository->findOneBy(['barecode' => $code]);
    }

    public function manualCreate(array $form): Ingredient
    {
        if ($this->manualProductExist($form['productName'])) {
            $newIngredient = $this->ingredientRepository->findOneBy(['name' => $form['productName']]);
        } else {
            $newIngredient = new Ingredient();
            $newIngredient->setName($form['productName'])
                        ->setBrand($form['productType'])
                        ->setBarecode('0000000000000');

            $this->entityManager->persist($newIngredient);
            $this->entityManager->flush();

            $newIngredient = $this->generateBarecode($newIngredient);
        }

        return $newIngredient;
    }

    public function generateBarecode(Ingredient $ingredient): Ingredient
    {
        $newBarecode = $ingredient->getId();

        for ($i = strlen(strval($newBarecode)); $i < 13; ++$i) {
            $newBarecode = '0'.$newBarecode;
        }

        $ingredient->setBarecode($newBarecode);

        $this->entityManager->flush();

        return $ingredient;
    }

    private function manualProductExist(string $name): bool
    {
        return $this->ingredientRepository->findOneBy(['name' => $name]) ? true : false;
    }
}
