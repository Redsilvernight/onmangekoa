<?php

namespace App\Services;

use App\Entity\ProductTag;
use App\Repository\ProductTagRepository;

class TagService
{
    public function __construct(
        private ProductTagRepository $productTagRepository
    ) {
    }

    public function createTag($tag): ProductTag
    {
        $existingTag = self::tagExist($tag);

        if (null != $existingTag) {
            return $existingTag;
        } else {
            $productTag = new ProductTag();
            $productTag->setName($tag);

            return $productTag;
        }
    }

    private function tagExist(string $tag): mixed
    {
        return $this->productTagRepository->findOneBy(['name' => $tag]);
    }
}
