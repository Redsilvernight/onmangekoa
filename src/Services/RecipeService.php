<?php

namespace App\Services;

use App\Entity\Inventory;
use App\Repository\InventoryRowRepository;

class RecipeService
{
    public function __construct(
        private InventoryRowRepository $rowRepository
    ) {
    }

    public function selectRandomProduct(Inventory $inventory): array
    {
        $allProduct = [];
        $listProduct = [];

        $allProduct = $this->rowRepository->findBy(['inventory' => $inventory]);

        foreach ($allProduct as $product) {
            if ($product->getIsOpen()) {
                array_push($listProduct, $product);
            }
        }

        return $listProduct;
    }
}
