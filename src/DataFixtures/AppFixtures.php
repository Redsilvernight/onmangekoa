<?php

namespace App\DataFixtures;

use App\Entity\Ingredient;
use App\Entity\Inventory;
use App\Entity\InventoryRow;
use App\Entity\User;
use App\Repository\IngredientRepository;
use App\Repository\InventoryRepository;
use App\Services\InventoryService;
use App\Services\ProductService;
use App\Services\TagService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    protected Generator $faker;

    public function __construct(
        private EntityManagerInterface $manager,
        private UserPasswordHasherInterface $hasheur,
        private ProductService $productService,
        private InventoryRepository $inventoryRepository,
        private IngredientRepository $ingredientRepository,
        private InventoryService $inventoryService,
        private TagService $tagService
    ) {
        $this->faker = Factory::create('fr_FR');
        $this->faker->addProvider(new \FakerRestaurant\Provider\fr_FR\Restaurant($this->faker));
        $this->faker->addProvider(new \Smknstd\FakerPicsumImages\FakerPicsumImagesProvider($this->faker));
    }

    public function load(ObjectManager $manager): void
    {
        $this->loadInventory();
        $this->loadUser();
        $this->loadIngredient();
        $this->loadContentInventory();
        $this->loadTag();
        $this->loadAdmin();
    }

    private function loadUser(): void
    {
        $listInventory = $this->inventoryRepository->findAll();

        for ($i = 0; $i < 10; ++$i) {
            $user = new User();
            $user->setEmail('exemple'.$i.'@example.com')
            ->setPassword($this->hasheur->hashPassword($user, 'password'))
            ->setRoles(['ROLE_USER'])
            ->setVerified(true)
            ->setInventory($listInventory[$i]);

            $this->manager->persist($user);
            $this->manager->flush();
        }
    }

    private function loadIngredient(): void
    {
        for ($i = 0; $i < 100; ++$i) {
            $ingredient = new Ingredient();
            $ingredient->setBrand($this->faker->company())
                    ->setName($this->faker->foodName()) //@phpstan-ignore method.notFound
                    ->setDescription($this->faker->paragraph())
                    ->setImageTemp($this->faker->imageUrl(width: 600, height: 600));

            $this->manager->persist($ingredient);
            $this->manager->flush();

            $this->productService->generateBarecode($ingredient);
        }

        for ($i = 0; $i < 100; ++$i) {
            $ingredient = new Ingredient();
            $ingredient->setBrand($this->faker->company())
                    ->setName($this->faker->vegetableName()) //@phpstan-ignore method.notFound
                    ->setDescription($this->faker->paragraph())
                    ->setImageTemp($this->faker->imageUrl(width: 600, height: 600));

            $this->manager->persist($ingredient);
            $this->manager->flush();

            $this->productService->generateBarecode($ingredient);
        }

        for ($i = 0; $i < 100; ++$i) {
            $ingredient = new Ingredient();
            $ingredient->setBrand($this->faker->company())
                    ->setName($this->faker->meatName()) //@phpstan-ignore method.notFound
                    ->setDescription($this->faker->paragraph())
                    ->setImageTemp($this->faker->imageUrl(width: 600, height: 600));

            $this->manager->persist($ingredient);
            $this->manager->flush();

            $this->productService->generateBarecode($ingredient);
        }
    }

    private function loadInventory(): void
    {
        for ($i = 0; $i < 10; ++$i) {
            $inventory = new Inventory();
            $inventory->setInvitationCode($this->inventoryService->generateCode($inventory))
                    ->setLimitDate(7);

            $this->manager->persist($inventory);
        }

        $this->manager->flush();
    }

    private function loadContentInventory(): void
    {
        $listInventory = $this->inventoryRepository->findAll();
        $listProduct = $this->ingredientRepository->findAll();

        for ($i = 0; $i < 9; ++$i) {
            for ($p = 0; $p < random_int(50, 100); ++$p) {
                $inventoryRow = new InventoryRow();
                $inventoryRow->setDlc(new \DateTime())
                            ->setIsBase(random_int(0, 1) == 0 ? false : true)
                            ->setIsOpen(random_int(0, 1) == 0 ? false : true)
                            ->setStock(random_int(1, 10))
                            ->setUnitStock('Pièce (P)')
                            ->setInventory($listInventory[$i])
                            ->setIngredient($listProduct[random_int(0, 299)])
                            ->setDlc(self::randomDlc(new \DateTime('now'), new \DateTime('14-10-2025')));

                $this->manager->persist($inventoryRow);
            }
        }

        $this->manager->flush();
    }

    private function loadTag(): void
    {
        $listIngredient = $this->ingredientRepository->findAll();

        foreach ($listIngredient as $ingredient) {
            for ($p = 0; $p < random_int(3, 7); ++$p) {
                $tag = $this->tagService->createTag($this->faker->dairyName()); //@phpstan-ignore method.notFound

                $this->manager->persist($tag);
            }
            $this->manager->flush();

            $ingredient->addTag($tag);
        }

        $this->manager->flush();
    }

    private function randomDlc(\DateTime $start, \DateTime $end): \DateTime
    {
        $randomTimestamp = mt_rand($start->getTimestamp(), $end->getTimestamp());
        $randomDate = new \DateTime();
        $randomDate->setTimestamp($randomTimestamp);

        return $randomDate;
    }

    private function loadAdmin(): void
    {
        $listProduct = $this->ingredientRepository->findAll();

        $inventory = new Inventory();
        $inventory->setInvitationCode($this->inventoryService->generateCode($inventory));

        $this->manager->persist($inventory);

        $user = new User();
        $user->setEmail('redsilvernight@gmail.com')
            ->setPassword($this->hasheur->hashPassword($user, 'testtest'))
            ->setRoles(['ROLE_ADMIN', 'ROLE_USER'])
            ->setVerified(true)
            ->setInventory($inventory);

        $this->manager->persist($user);

        for ($p = 0; $p < random_int(50, 100); ++$p) {
            $inventoryRow = new InventoryRow();
            $inventoryRow->setDlc(new \DateTime())
                ->setIsBase(0 == random_int(0, 1) ? false : true)
                ->setIsOpen(0 == random_int(0, 1) ? false : true)
                ->setStock(random_int(1, 10))
                ->setUnitStock('Pièce (P)')
                ->setInventory($inventory)
                ->setIngredient($listProduct[random_int(0, 299)])
                ->setDlc(self::randomDlc(new \DateTime('now'), new \DateTime('14-10-2025')));

            $this->manager->persist($inventoryRow);
        }

        $this->manager->flush();
    }
}
