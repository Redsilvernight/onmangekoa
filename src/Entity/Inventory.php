<?php

namespace App\Entity;

use App\Repository\InventoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: InventoryRepository::class)]
class Inventory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\Length(min: 0, max: 30)]
    private ?int $limitDate = 7;

    #[ORM\Column(length: 15, nullable: true)]
    #[Assert\Length(min: 2, max: 15)]
    private ?string $invitationCode = null;

    /**
     * @var Collection<int, RequestInventory>
     */
    #[ORM\OneToMany(targetEntity: RequestInventory::class, mappedBy: 'inventory', orphanRemoval: true)]
    #[Assert\Type(RequestInventory::class)]
    private Collection $requestInventories;

    /**
     * @var Collection<int, User>
     */
    #[ORM\OneToMany(targetEntity: User::class, mappedBy: 'Inventory')]
    #[Assert\Type(User::class)]
    private Collection $users;

    /**
     * @var Collection<int, InventoryRow>
     */
    #[ORM\OneToMany(targetEntity: InventoryRow::class, mappedBy: 'inventory')]
    #[Assert\Type(InventoryRow::class)]
    private Collection $inventoryRows;

    public function __construct()
    {
        $this->requestInventories = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->inventoryRows = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLimitDate(): ?int
    {
        return $this->limitDate;
    }

    public function setLimitDate(?int $limitDate): static
    {
        $this->limitDate = $limitDate;

        return $this;
    }

    public function getInvitationCode(): ?string
    {
        return $this->invitationCode;
    }

    public function setInvitationCode(string $invitationCode): static
    {
        $this->invitationCode = $invitationCode;

        return $this;
    }

    /**
     * @return Collection<int, RequestInventory>
     */
    public function getRequestInventories(): Collection
    {
        return $this->requestInventories;
    }

    public function addRequestInventory(RequestInventory $requestInventory): static
    {
        if (!$this->requestInventories->contains($requestInventory)) {
            $this->requestInventories->add($requestInventory);
            $requestInventory->setInventory($this);
        }

        return $this;
    }

    public function removeRequestInventory(RequestInventory $requestInventory): static
    {
        if ($this->requestInventories->removeElement($requestInventory)) {
            // set the owning side to null (unless already changed)
            if ($requestInventory->getInventory() === $this) {
                $requestInventory->setInventory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): static
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setInventory($this);
        }

        return $this;
    }

    public function removeUser(User $user): static
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getInventory() === $this) {
                $user->setInventory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, InventoryRow>
     */
    public function getInventoryRows(): Collection
    {
        return $this->inventoryRows;
    }

    public function addInventoryRow(InventoryRow $inventoryRow): static
    {
        if (!$this->inventoryRows->contains($inventoryRow)) {
            $this->inventoryRows->add($inventoryRow);
            $inventoryRow->setInventory($this);
        }

        return $this;
    }

    public function removeInventoryRow(InventoryRow $inventoryRow): static
    {
        if ($this->inventoryRows->removeElement($inventoryRow)) {
            // set the owning side to null (unless already changed)
            if ($inventoryRow->getInventory() === $this) {
                $inventoryRow->setInventory(null);
            }
        }

        return $this;
    }
}
