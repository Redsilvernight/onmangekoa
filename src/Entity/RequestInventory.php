<?php

namespace App\Entity;

use App\Repository\RequestInventoryRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: RequestInventoryRepository::class)]
class RequestInventory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'requestInventories')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\Type(Inventory::class)]
    #[Assert\NotNull()]
    private ?Inventory $inventory = null;

    #[ORM\OneToOne()]
    #[ORM\JoinColumn(nullable: false, onDelete: 'cascade')]
    #[Assert\Type(User::class)]
    #[Assert\NotNull()]
    private ?User $user = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInventory(): ?Inventory
    {
        return $this->inventory;
    }

    public function setInventory(?Inventory $inventory): static
    {
        $this->inventory = $inventory;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): static
    {
        $this->user = $user;

        return $this;
    }
}
