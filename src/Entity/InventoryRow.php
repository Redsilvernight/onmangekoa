<?php

namespace App\Entity;

use App\Repository\InventoryRowRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: InventoryRowRepository::class)]
class InventoryRow
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Assert\NotNull()]
    private ?\DateTimeInterface $dlc = null;

    #[ORM\Column]
    #[Assert\NotNull()]
    #[Assert\Positive()]
    private ?float $stock = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotNull()]
    #[Assert\Length(min: 1, max: 15)]
    private ?string $unitStock = null;

    #[ORM\Column]
    private ?bool $isOpen = null;

    #[ORM\ManyToOne]
    #[Assert\Type(Ingredient::class)]
    private ?Ingredient $ingredient = null;

    #[ORM\ManyToOne(inversedBy: 'inventoryRows')]
    #[Assert\Type(Inventory::class)]
    private ?Inventory $inventory = null;

    #[ORM\Column(nullable : true)]
    private ?bool $isBase = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDlc(): ?\DateTimeInterface
    {
        return $this->dlc;
    }

    public function setDlc(?\DateTimeInterface $dlc): static
    {
        $this->dlc = $dlc;

        return $this;
    }

    public function getStock(): ?float
    {
        return $this->stock;
    }

    public function setStock(float $stock): static
    {
        $this->stock = $stock;

        return $this;
    }

    public function getUnitStock(): ?string
    {
        return $this->unitStock;
    }

    public function setUnitStock(string $unitStock): static
    {
        $this->unitStock = $unitStock;

        return $this;
    }

    public function getIsOpen(): ?bool
    {
        return $this->isOpen;
    }

    public function setIsOpen(bool $isOpen): static
    {
        $this->isOpen = $isOpen;

        return $this;
    }

    public function getIngredient(): ?Ingredient
    {
        return $this->ingredient;
    }

    public function setIngredient(?Ingredient $ingredient): static
    {
        $this->ingredient = $ingredient;

        return $this;
    }

    public function getInventory(): ?Inventory
    {
        return $this->inventory;
    }

    public function setInventory(?Inventory $inventory): static
    {
        $this->inventory = $inventory;

        return $this;
    }

    public function getIsBase(): ?bool
    {
        return $this->isBase;
    }

    public function setIsBase(bool $isBase): static
    {
        $this->isBase = $isBase;

        return $this;
    }
}
