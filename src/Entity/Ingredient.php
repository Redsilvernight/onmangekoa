<?php

namespace App\Entity;

use App\Repository\IngredientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: IngredientRepository::class)]
class Ingredient
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotNull()]
    #[Assert\Length(min: 2, max: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotNull()]
    #[Assert\Length(min: 2, max: 255)]
    private ?string $brand = null;

    #[ORM\Column(nullable: true)]
    #[Assert\NotNull()]
    #[Assert\Length(min: 2, max: 15)]
    private ?string $barecode = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Assert\Length(min: 0, max: 255)]
    private ?string $description = null;

    /**
     * @var Collection<int, ProductTag>
     */
    #[ORM\ManyToMany(targetEntity: ProductTag::class, cascade: ['persist'])]
    private Collection $tags;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $imageTemp = null;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(string $brand): static
    {
        $this->brand = $brand;

        return $this;
    }

    public function getBarecode(): ?string
    {
        return $this->barecode;
    }

    public function setBarecode(?string $barecode): static
    {
        $this->barecode = $barecode;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, ProductTag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(ProductTag $tag): static
    {
        if (!$this->tags->contains($tag)) {
            $this->tags->add($tag);
        }

        return $this;
    }

    public function removeTag(ProductTag $tag): static
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    public function getImageTemp(): ?string
    {
        return $this->imageTemp;
    }

    public function setImageTemp(?string $imageTemp): static
    {
        $this->imageTemp = $imageTemp;

        return $this;
    }
}
