<?php

namespace App\Form;

use App\Entity\InventoryRow;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class InventoryRowType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('dlc', DateTimeType::class, [
            ])
            ->add('isBase', CheckboxType::class, [
                'required' => false,
            ])
            ->add('stock', NumberType::class, [
                'constraints' => [
                    new Length(['min' => 0, 'max' => 100000]),
                    new NotBlank(),
                ],
            ])
            ->add('unitStock', ChoiceType::class, [
                'choices' => [
                    'Gramme (g)' => 1,
                    'Litre (l)' => 2,
                    'Pièce (p)' => 3,
                ],
                'constraints' => [
                    new NotBlank(),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => InventoryRow::class,
        ]);
    }
}
