<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ManualProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('productName', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'minlength' => '2',
                    'maxlength' => '50',
                ],
                'label' => 'Nom du produit',
                'label_attr' => [
                    'class' => 'form-label mt-4',
                ],
                'constraints' => [
                    new Length(['min' => 2, 'max' => 50, 'minMessage' => 'Le produit doit avoir plus de 2 caractères.', 'maxMessage' => 'Le produit ne peut pas contenir plus de 50 caractères.']),
                    new NotBlank(['message' => 'Le nom du produit ne peut pas être vide.']),
                ],
            ])
            ->add('productType', ChoiceType::class, [
                'choices' => [
                    'Fruit' => 'Fruit',
                    'Légume' => 'Légume',
                    'Viande' => 'Viande',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Le type du produit ne peut pas être vide.',
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
