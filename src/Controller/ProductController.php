<?php

namespace App\Controller;

use App\Entity\InventoryRow;
use App\Form\InventoryRowType;
use App\Form\ManualProductType;
use App\Services\ProductService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class ProductController extends AbstractController
{
    public function __construct(
        private ProductService $productService,
        private EntityManagerInterface $entityManager,
    ) {
    }

    #[Route('/product/add/scan', name: 'app_product_scan', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function scan(): Response
    {
        return $this->render('product/scan.html.twig');
    }

    #[Route('/product/detect/{code}', name: 'app_product_detect', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER')]
    public function detect(string $code, Request $request): Response
    {
        $existingProduct = $this->productService->productExist($code);
        if (null == $existingProduct) {
            $productInfo = $this->productService->detect($code);

            $scanProduct = $this->productService->createProduct($productInfo);
        } else {
            $scanProduct = $existingProduct;
        }

        $inventoryRow = new InventoryRow();
        $form = $this->createForm(InventoryRowType::class, $inventoryRow);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $inventoryRow->setIsOpen(false)
                        ->setIngredient($scanProduct);

            $this->getUser()->getInventory()->addInventoryRow($inventoryRow);
            $this->entityManager->persist($inventoryRow);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_inventory_content');
        }

        return $this->render('product/profil.html.twig', [
            'ingredient' => $scanProduct,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/product/add/manual', name: 'app_product_manual_add', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER')]
    public function manualAdd(Request $request): Response
    {
        $form = $this->createForm(ManualProductType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newIngredient = $this->productService->manualCreate($form->getData());

            return $this->redirectToRoute('app_product_detect', ['code' => $newIngredient->getBarecode()]);
        }

        return $this->render('product/manual_add.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
