<?php

namespace App\Controller;

use App\Entity\RequestInventory;
use App\Repository\InventoryRepository;
use App\Repository\RequestInventoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class RequestController extends AbstractController
{
    public function __construct(
        private RequestInventoryRepository $requestInventoryRepository,
        private EntityManagerInterface $entityManager,
        private InventoryRepository $inventoryRepository
    ) {
    }

    #[Route('/request/send', name: 'app_request_send', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function listSendRequest(): Response
    {
        $requestSending = $this->requestInventoryRepository->findBy(['user' => $this->getUser()]);

        return $this->render('request/sending.html.twig', [
            'requestSending' => $requestSending,
        ]);
    }

    #[Route('/request/received', name: 'app_request_receive', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function listReceiveRequest(): Response
    {
        $requestReceived = $this->requestInventoryRepository->findBy(['inventory' => $this->getUser()->getInventory()]);

        return $this->render('request/receive.html.twig', [
            'requestReceived' => $requestReceived,
        ]);
    }

    #[Route('/request/cancel/{id}', name: 'app_request_cancel', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER')]
    public function cancelRequest(RequestInventory $requestInventory): Response
    {
        $this->entityManager->remove($requestInventory);
        $this->entityManager->flush();

        return $this->redirectToRoute('app_request_receive');
    }

    #[Route('/request/accept/{id}', name: 'app_request_accept', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER')]
    public function acceptRequest(RequestInventory $requestInventory): Response
    {
        $removedInventory = $this->inventoryRepository->find($requestInventory->getUser()->getInventory()->getId());
        $requestInventory->getUser()->setInventory($requestInventory->getInventory());

        $this->entityManager->flush();

        // return $this->redirectToRoute('app_request_cancel', ['id' => $requestInventory->getId()]);
        return $this->redirectToRoute('app_inventory_delete', ['requestInventory' => $requestInventory->getId(), 'inventory' => $removedInventory->getId()]);
    }
}
