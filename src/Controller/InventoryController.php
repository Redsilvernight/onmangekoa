<?php

namespace App\Controller;

use App\Entity\InventoryRow;
use App\Entity\RequestInventory;
use App\Entity\User;
use App\Form\DlcLimitType;
use App\Form\EditInventoryRowType;
use App\Form\RequestInventoryType;
use App\Repository\InventoryRepository;
use App\Repository\InventoryRowRepository;
use App\Repository\RequestInventoryRepository;
use App\Services\InventoryService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class InventoryController extends AbstractController
{
    public function __construct(
        private InventoryRepository $inventoryRepository,
        private InventoryRowRepository $rowRepository,
        private EntityManagerInterface $manager,
        private RequestInventoryRepository $requestInventoryRepository,
        private InventoryService $inventoryService
    ) {
    }

    #[Route('/inventory/join', name: 'app_inventory_join', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER')]
    public function join(Request $request): Response
    {
        $form = $this->createForm(RequestInventoryType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $inventory = $this->inventoryRepository->findOneby(['invitationCode' => $form->getData()]);

            $requestInventory = new RequestInventory();
            $requestInventory->setInventory($inventory)
                            ->setUser($this->getUser());

            $this->manager->persist($requestInventory);
            $this->manager->flush();

            return $this->redirectToRoute('app_request_send');
        }

        return $this->render('inventory/join.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/inventory/remove/{requestInventory}/{inventory}', name: 'app_inventory_delete', methods: ['GET', 'DELETE'])]
    #[IsGranted('ROLE_USER')]
    public function remove(int $requestInventory, int $inventory): Response
    {
        $requestInventory = $this->requestInventoryRepository->find($requestInventory);
        $inventory = $this->inventoryRepository->find($inventory);

        $this->manager->remove($inventory);
        $this->manager->flush();

        return $this->redirectToRoute('app_request_cancel', ['id' => $requestInventory->getId()]);
    }

    #[Route(path: '/inventory/user', name: 'app_inventory_user', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function listUser(): Response
    {
        return $this->render('inventory/list_user.html.twig', [
            'userList' => $this->inventoryService->listUser($this->getUser()),
        ]);
    }

    #[Route(path: '/inventory/ousted/{id}', name: 'app_inventory_ousted', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function oustedUser(User $user): Response
    {
        $this->inventoryService->removeUser($user);

        return $this->redirectToRoute('app_inventory_user');
    }

    #[Route(path: '/inventory/content', name: 'app_inventory_content', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function listContent(): Response
    {
        return $this->render('inventory/content.html.twig', [
            'inventoryContent' => $this->rowRepository->findBy(['inventory' => $this->getUser()->getInventory()]),
        ]);
    }

    #[Route(path: '/inventory/remove/{row}', name: 'app_inventory_remove', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function removeInventoryRow(InventoryRow $row): Response
    {
        $this->manager->remove($row);
        $this->manager->flush();

        return $this->redirectToRoute('app_inventory_content');
    }

    #[Route(path: '/inventory/edit/{row}', name: 'app_inventory_edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER')]
    public function editInventoryRow(InventoryRow $row, Request $request): Response
    {
        $form = $this->createForm(EditInventoryRowType::class, $row);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->flush();

            return $this->redirectToRoute('app_inventory_content');
        }

        return $this->render('inventory/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/inventory/limit', name: 'app_inventory_limit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER')]
    public function editLimitDate(Request $request): Response
    {
        $form = $this->createForm(DlcLimitType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->inventoryService->setLimitDate($this->getUser()->getInventory(), $form->get('limitDate')->getData());

            return $this->render('security/setting.html.twig');
        }

        return $this->render('inventory/limit.html.twig', [
            'form' => $form->createView(),
        ]);

    }
}
