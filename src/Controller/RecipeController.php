<?php

namespace App\Controller;

use App\Services\RecipeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class RecipeController extends AbstractController
{
    public function __construct(
        private RecipeService $recipeService
    ) {
    }

    #[Route('/recipe', name: 'app_recipe', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function index(): Response
    {
        return $this->render('recipe/index.html.twig');
    }

    #[Route('/recipe/random', name: 'app_recipe_random', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function randomRecipe(): Response
    {
        $slectedIngredient = $this->recipeService->selectRandomProduct($this->getUser()->getInventory());

        return $this->render('recipe/random.html.twig');
    }

    #[Route('/recipe/perso', name: 'app_recipe_perso', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function persoRecipe(): Response
    {
        return $this->render('recipe/perso.html.twig');
    }
}
