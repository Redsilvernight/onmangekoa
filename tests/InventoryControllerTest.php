<?php

namespace App\Tests;

use App\Entity\Inventory;
use App\Entity\User;
use App\Repository\InventoryRowRepository;
use App\Services\InventoryService;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class InventoryControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private InventoryService $inventoryService;
    private string $inventoryUser;
    private User $user;
    private User $userTest;
    private InventoryRowRepository $rowRepository;


    protected function setUp(): void
    {
        $this->client = static::createClient();
        $container = static::getContainer();
        $em = $container->get('doctrine.orm.entity_manager');

        $this->inventoryService = $container->get(InventoryService::class);
        $this->rowRepository = $container->get(InventoryRowRepository::class);

        $userRepository = $em->getRepository(User::class);

        // Remove any existing users from the test database
        foreach ($userRepository->findAll() as $user) {
            $em->remove($user);
        }

        $em->flush();

        // Create a User fixture
        /** @var UserPasswordHasherInterface $passwordHasher */
        $passwordHasher = $container->get('security.user_password_hasher');

        $this->user = (new User())->setEmail('email@example.com');
        $this->user->setPassword($passwordHasher->hashPassword($this->user, 'password'))
            ->setInventory($this->inventoryService->createInventory());

        $em->persist($this->user);

        $this->userTest = (new User())->setEmail('emailTest@example.com');
        $this->userTest->setPassword($passwordHasher->hashPassword($this->userTest, 'passwordTest'))
        ->setInventory($this->inventoryService->createInventory());

        $em->persist($this->userTest);

        $em->flush();

        $this->inventoryUser = $this->user->getInventory()->getInvitationCode();

        $this->client->request('GET', '/');
        $this->client->followRedirect();

        $this->client->submitForm('Sign in', [
            '_username' => 'emailTest@example.com',
            '_password' => 'passwordTest',
        ]);

        $this->client->followRedirect();
    }

    private function joinInventory()
    {
        $this->client->request('GET', '/inventory/join');
        self::assertResponseIsSuccessful();
        $this->client->submitForm('Rejoindre le foyer', [
            'request_inventory[inventory]' => $this->inventoryUser,
        ]);

        self::assertResponseRedirects('/request/send');
        $this->client->followRedirect();
        self::assertSelectorTextContains('a', 'Annuler la demande');

        $this->client->request('GET', '/logout');
    }

    private function acceptJoin()
    {
        $this->client->followRedirects();
        $crawler = $this->client->request('GET', '/request/received');
        self::assertSelectorTextContains('h1', 'Request received');
        self::assertCount(1, $crawler->filter('.userEmail'));

        $this->client->clickLink('Accepter la demande');
        self::assertSelectorTextContains('h1', 'Request received');
    }

    public function testJoin()
    {
        self::joinInventory();

        $this->client->loginUser($this->user);
        self::assertResponseRedirects('/');

        self::acceptJoin();

        $crawler = $this->client->request('GET', '/inventory/user');
        self::assertResponseIsSuccessful();
        self::assertCount(2, $crawler->filter('.userEmail'));
    }

    public function testRemove()
    {
        self::joinInventory();

        $this->client->loginUser($this->user);

        $this->client->followRedirect();
        $crawler = $this->client->request('GET', '/request/received');
        self::assertSelectorTextContains('h1', 'Request received');
        self::assertCount(1, $crawler->filter('.userEmail'));

        $link = $crawler->selectLink('Annuler la demande')->link();
        $crawler = $this->client->click($link);
        $this->client->followRedirect();

        self::assertSelectorTextContains('h1', 'Request received');
        self::assertCount(0, $crawler->filter('.div-received'));

        $crawler = $this->client->request('GET', '/inventory/user');
        self::assertResponseIsSuccessful();
        self::assertCount(1, $crawler->filter('.userEmail'));
    }

    public function testOustedUser()
    {
        self::testJoin();

        $this->client->loginUser($this->userTest);

        $crawler = $this->client->request('GET', '/inventory/user');

        self::assertResponseIsSuccessful();
        self::assertCount(2, $crawler->filter('.userEmail'));

        $link = $crawler->selectLink('Retirer du foyer')->link();
        $crawler = $this->client->click($link);
        $this->client->followRedirects();

        self::assertCount(1, $crawler->filter('.userEmail'));
    }

    public function testListContent()
    {
        $this->client->loginUser($this->user);

        $this->client->request('GET', '/inventory/content');

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('h1', 'Content inventory');
    }

    public function testRemoveInventoryRow()
    {
        $this->client->loginUser($this->user);

        $this->client->request('GET', '/product/add/manual');
        self::assertResponseIsSuccessful();

        $this->client->submitForm('Ajouter le produit', [
            'manual_product[productName]' => 'productTest',
            'manual_product[productType]' => 'Légume',
        ]);

        $this->client->followRedirect();

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('h1', 'Légume - productTest');

        $this->client->submitForm('Ajouter', [
            'inventory_row[dlc]' => '2024-07-09T17:54',
            'inventory_row[isBase]' => 1,
            'inventory_row[stock]' => 10,
            'inventory_row[unitStock]' => '3'
        ]);

        $crawler = $this->client->followRedirect();

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('h1', 'Content inventory');
        self::assertCount(1, $this->rowRepository->findBy(['inventory' => $this->user->getInventory()]));

        $link = $crawler->selectLink("Supprimer de l'inventaire")->link();
        $crawler = $this->client->click($link);


        $this->client->followRedirect();

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('h1', 'Content inventory');
        self::assertCount(0, $this->rowRepository->findBy(['inventory' => $this->user->getInventory()]));
    }

    public function testEditInventoryRow() {
        $this->client->loginUser($this->user);

        $this->client->request('GET', '/product/add/manual');
        self::assertResponseIsSuccessful();

        $this->client->submitForm('Ajouter le produit', [
            'manual_product[productName]' => 'productTest',
            'manual_product[productType]' => 'Légume',
        ]);

        $this->client->followRedirect();

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('h1', 'Légume - productTest');

        $this->client->submitForm('Ajouter', [
            'inventory_row[dlc]' => '2024-07-09T17:54',
            'inventory_row[isBase]' => 1,
            'inventory_row[stock]' => 10,
            'inventory_row[unitStock]' => '3'
        ]);

        $crawler = $this->client->followRedirect();

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('h1', 'Content inventory');
        self::assertCount(1, $this->rowRepository->findBy(['inventory' => $this->user->getInventory()]));

        $link = $crawler->selectLink("Editer")->link();
        $crawler = $this->client->click($link);

        $this->client->submitForm('Editer', [
            'edit_inventory_row[stock]' => '99',
        ]);

        $this->client->followRedirect();

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('h1', 'Content inventory');
        self::assertSelectorTextSame('span', 'x99');
    }

    public function testSetting()
    {
        $this->client->loginUser($this->user);

        $crawler = $this->client->request('GET', '/settings');
        self::assertResponseIsSuccessful();

        $link = $crawler->selectLink("Modifier la limite")->link();
        $crawler = $this->client->click($link);
        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('h1', 'Modifier la limite');

        $this->client->submitForm('Définir la limite', [
            'dlc_limit[limitDate]' => '15',
        ]);
        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('p', 'Limite de consommation : 15 jours');
    }
}
