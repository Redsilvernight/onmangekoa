<?php

namespace App\Tests;

use App\Entity\User;
use App\Repository\InventoryRowRepository;
use App\Services\InventoryService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private InventoryService $inventoryService;
    private User $user;
    private InventoryRowRepository $rowRepository;
    protected function setUp(): void
    {
        $this->client = static::createClient();
        $container = static::getContainer();
        $em = $container->get('doctrine.orm.entity_manager');

        $this->inventoryService = $container->get(InventoryService::class);
        $this->rowRepository = $container->get(InventoryRowRepository::class);

        $userRepository = $em->getRepository(User::class);

        // Remove any existing users from the test database
        foreach ($userRepository->findAll() as $user) {
            $em->remove($user);
        }

        $em->flush();

        // Create a User fixture
        /** @var UserPasswordHasherInterface $passwordHasher */
        $passwordHasher = $container->get('security.user_password_hasher');

        $this->user = (new User())->setEmail('email@example.com');
        $this->user->setPassword($passwordHasher->hashPassword($this->user, 'password'))
        ->setInventory($this->inventoryService->createInventory());

        $em->persist($this->user);
        $em->flush();

        $this->client->request('GET', '/');
        $this->client->followRedirect();

        $this->client->submitForm('Sign in', [
            '_username' => 'email@example.com',
            '_password' => 'password',
        ]);

        $this->client->followRedirect();
    }

    public function testScan() 
    {
        $this->client->loginUser($this->user);

        $this->client->request('GET', '/product/add/scan');

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('button', 'Start');
    }

    public function testDetect()
    {
        $this->client->loginUser($this->user);

        $this->client->request('GET', '/product/detect/4260122392049');

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('h1', 'True Fruits - Smoothie triple pink');

        $this->client->submitForm('Ajouter', [
            'inventory_row[dlc]' => '2024-07-09T17:54',
            'inventory_row[isBase]' => 1,
            'inventory_row[stock]' => 10,
            'inventory_row[unitStock]' => '3'
        ]);

        $this->client->followRedirect();

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('h1', 'Content inventory');
        self::assertCount(1, $this->rowRepository->findBy(['inventory' => $this->user->getInventory()]));

    }

    public function testManualAdd()
    {
        $this->client->loginUser($this->user);

        $this->client->request('GET', '/product/add/manual');
        self::assertResponseIsSuccessful();

        $this->client->submitForm('Ajouter le produit', [
            'manual_product[productName]' => 'productTest',
            'manual_product[productType]' => 'Légume',
        ]);

        $this->client->followRedirect();

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('h1', 'Légume - productTest');

        $this->client->submitForm('Ajouter', [
            'inventory_row[dlc]' => '2024-07-09T17:54',
            'inventory_row[isBase]' => 1,
            'inventory_row[stock]' => 10,
            'inventory_row[unitStock]' => '3'
        ]);

        $this->client->followRedirect();

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('h1', 'Content inventory');
        self::assertCount(1, $this->rowRepository->findBy(['inventory' => $this->user->getInventory()]));
    }
}
