#---Symfony-And-Docker-Makefile---------------#
# Author: https://github.com/yoanbernabeu
# License: MIT
#---------------------------------------------#

#---VARIABLES---------------------------------#
#---DOCKER---#
DOCKER = docker
DOCKER_RUN = $(DOCKER) run
DOCKER_COMPOSE = docker compose
DOCKER_COMPOSE_UP = $(DOCKER_COMPOSE) up -d
DOCKER_COMPOSE_STOP = $(DOCKER_COMPOSE) stop
#------------#

#---SYMFONY--#
SYMFONY = symfony
SYMFONY_SERVER_START = $(SYMFONY) serve -d
SYMFONY_SERVER_STOP = $(SYMFONY) server:stop
SYMFONY_CONSOLE = $(SYMFONY) console
SYMFONY_LINT = $(SYMFONY_CONSOLE) lint:
#------------#

#---COMPOSER-#
COMPOSER = composer
COMPOSER_INSTALL = $(COMPOSER) install
COMPOSER_UPDATE = $(COMPOSER) update
#------------#

#---NPM-----#
NPM = npm
NPM_INSTALL = $(NPM) install --force
NPM_UPDATE = $(NPM) update
NPM_BUILD = $(NPM) run build
NPM_DEV = $(NPM) run dev
NPM_WATCH = $(NPM) run watch
#------------#

#---PHPQA---#
PHPQA = jakzal/phpqa:php8.3
PHPQA_RUN = $(DOCKER_RUN) --init --rm -v $(PWD):/project -w /project $(PHPQA)
#------------#

#---PHPUNIT-#
PHPUNIT = APP_ENV=test $(SYMFONY) php bin/phpunit
#------------#
#---------------------------------------------#

## === 🆘  HELP ==================================================
help: ## Show this help.
	@echo "Symfony-And-Docker-Makefile"
	@echo "---------------------------"
	@echo "Usage: make [target]"
	@echo ""
	@echo "Targets:"
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
#---------------------------------------------#

## === 🐋  DOCKER ================================================
docker-up: ## Start docker containers.
	$(DOCKER_COMPOSE_UP)
.PHONY: docker-up

docker-stop: ## Stop docker containers.
	$(DOCKER_COMPOSE_STOP)
.PHONY: docker-stop
#---------------------------------------------#

## === 🎛️  SYMFONY ===============================================
sf: ## List and Use All Symfony commands (make sf command="commande-name").
	$(SYMFONY_CONSOLE) $(command)
.PHONY: sf

start: ## Start symfony server.
	$(SYMFONY_SERVER_START)
.PHONY: start

stop: ## Stop symfony server.
	$(SYMFONY_SERVER_STOP)
.PHONY: stop

clear-cache: ## Clear symfony cache.
	$(SYMFONY_CONSOLE) cache:clear
.PHONY: clear-cache

sf-log: ## Show symfony logs.
	$(SYMFONY) server:log
.PHONY: sf-log

database: ## Create symfony database.
	$(SYMFONY_CONSOLE) doctrine:database:create --if-not-exists
.PHONY: new-db

sf-dd: ## Drop symfony database.
	$(SYMFONY_CONSOLE) doctrine:database:drop --if-exists --force
.PHONY: sf-dd

maj-schema: ## Update symfony schema database.
	$(SYMFONY_CONSOLE) doctrine:schema:update --force
	$(SYMFONY_CONSOLE) doctrine:schema:update --force --env=test
.PHONY: maj-schema

maj-schema-test: ## Update symfony test schema database.
	$(SYMFONY_CONSOLE) doctrine:schema:update --force --env=test
.PHONY: maj-schema-test

migration: ## Make migrations.
	$(SYMFONY_CONSOLE) make:migration
.PHONY: migration

migrate: ## Migrate.
	$(SYMFONY_CONSOLE) doctrine:migrations:migrate --no-interaction
.PHONY: migrate

load-fixtures: ## Load fixtures.
	$(SYMFONY_CONSOLE) doctrine:fixtures:load --no-interaction
.PHONY: load-fixtures

entity: ## Make symfony entity
	$(SYMFONY_CONSOLE) make:entity
.PHONY: entity

controller: ## Make symfony controller
	$(SYMFONY_CONSOLE) make:controller
.PHONY: controller

form: ## Make symfony Form
	$(SYMFONY_CONSOLE) make:form
.PHONY: form

fix-perm: ## Fix permissions.
	chmod -R 777 var
.PHONY: fix-perm

sudo-fix-perm: ## Fix permissions with sudo.
	sudo chmod -R 777 var
.PHONY: sudo-fix-perm

dump-env: ## Dump env.
	$(SYMFONY_CONSOLE) debug:dotenv
.PHONY: dump-env

sf-dump-env-container: ## Dump Env container.
	$(SYMFONY_CONSOLE) debug:container --env-vars
.PHONY: sf-dump-env-container

sf-dump-routes: ## Dump routes.
	$(SYMFONY_CONSOLE) debug:router
.PHONY: sf-dump-routes

open: ## Open project in a browser.
	$(SYMFONY) open:local
.PHONY: open

sf-open-email: ## Open Email catcher.
	$(SYMFONY) open:local:webmail
.PHONY: sf-open-email

sf-check-requirements: ## Check requirements.
	$(SYMFONY) check:requirements
.PHONY: sf-check-requirements
#---------------------------------------------#

## === 📦  COMPOSER ==============================================
cp-install: ## Install composer dependencies.
	$(COMPOSER_INSTALL)
.PHONY: cp-install

cp-update: ## Update composer dependencies.
	$(COMPOSER_UPDATE)$(PHPUNIT) --testdox
.PHONY: cp-update

composer-validate: ## Validate composer.json file.
	$(COMPOSER) validate
.PHONY: composer-validate

composer-validate-deep: ## Validate composer.json and composer.lock files in strict mode.
	$(COMPOSER) validate --strict --check-lock
.PHONY: composer-validate-deep
#---------------------------------------------#

## === 📦  NPM ===================================================
npm-install: ## Install npm dependencies.
	$(NPM_INSTALL)
.PHONY: npm-install

npm-update: ## Update npm dependencies.
	$(NPM_UPDATE)
.PHONY: npm-update

npm-build: ## Build assets.
	$(NPM_BUILD)
.PHONY: npm-build

npm-dev: ## Build assets in dev mode.
	$(NPM_DEV)
.PHONY: npm-dev

npm-watch: ## Watch assets.
	$(NPM_WATCH)
.PHONY: npm-watch
#---------------------------------------------#

## === 🐛  PHPQA =================================================
fixer-dry-run: ## Run php-cs-fixer in dry-run mode.
	$(PHPQA_RUN) php-cs-fixer fix src --rules=@Symfony --verbose --dry-run
.PHONY: fixer-dry-run

fixer: ## Run php-cs-fixer.
	./tools/php-cs-fixer/vendor/bin/php-cs-fixer fix src --rules=@Symfony --verbose
.PHONY: fixer

phpstan: ## Run phpstan.
	./vendor/bin/phpstan analyse src public templates --level=6
.PHONY: phpstan

security-checker: ## Run security-checker.
	$(SYMFONY) security:check
.PHONY: security-checker

phpcpd: ## Run phpcpd (copy/paste detector).
	$(PHPQA_RUN) phpcpd ./src
.PHONY: phpcpd

metrics: ## Run php-metrics.
	$(PHPQA_RUN) phpmetrics --report-html=var/phpmetrics ./src
.PHONY: metrics

lint-twigs: ## Lint twig files.
	$(SYMFONY_LINT)twig ./templates
.PHONY: lint-twigs

lint-yaml: ## Lint yaml files.
	$(SYMFONY_LINT)yaml ./config
.PHONY: lint-yaml

lint-container: ## Lint container.
	bin/console lint:container --no-debug
.PHONY: lint-container

lint-schema: ## Lint Doctrine schema.
	$(SYMFONY_CONSOLE) doctrine:schema:validate --skip-sync -vvv --no-interaction
.PHONY: lint-schema

audit: ## Run composer audit.
	$(COMPOSER) audit
.PHONY: audit
#---------------------------------------------#

## === 🔎  TESTS =================================================
tests: ## Run tests.
	bin/phpunit 
.PHONY: tests

tests-coverage: ## Run tests with coverage.
	XDEBUG_MODE=coverage $(PHPUNIT) --coverage-html var/coverage
.PHONY: tests-coverage
#---------------------------------------------#

## === ⭐  OTHERS =================================================
before-commit: fixer phpstan security-checker lint-twigs lint-yaml lint-container lint-schema npm-build tests ## Run before commit.
.PHONY: before-commit

first-install: cp-install npm-install npm-build fix-perm database migrate start open ## First install.
.PHONY: first-install

start-app: docker-up start open npm-build ## Start project.
.PHONY: start

stop: stop ## Stop project.
.PHONY: stop

reset-db: ## Reset database.
	$(eval CONFIRM := $(shell read -p "Are you sure you want to reset the database? [y/N] " CONFIRM && echo $${CONFIRM:-N}))
	@if [ "$(CONFIRM)" = "y" ]; then \
		$(MAKE) sf-dd; \
		$(MAKE) database; \
		$(MAKE) migrate; \
	fi
.PHONY: reset-db
#---------------------------------------------#
